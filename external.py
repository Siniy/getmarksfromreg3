import logging
import xml.etree.ElementTree as ET

import requests
import uuid
import re
import traceback
from utils import prettify, app_log as logger


def get_utm_documents(service_url, doc_type):
    """
    Получает список ссылок на документы определенного типа
    :param service_url: Адрес сервиса
    :param doc_type: Тип документа (Waybill_v3 / FORM2REGINFO)
    :return: список url
    """
    urls = []
    try:
        all_urls_doc = requests.get(service_url, timeout=5)
        for child in ET.fromstring(all_urls_doc.text):
            if doc_type.upper() in child.text.upper():
                urls.append(child.text)
        return {"utm_connected": True, "urls": urls}
    except Exception:
        err_stack = traceback.format_exc()
        logger.error(err_stack)
        return {"utm_connected": False, "urls": urls}


def get_fsrar_id_from_utm(utm_url):
    """
    Забирает FSRAR_ID со страницы UTM
    :param utm_url:
    :return: строка - FSRAR_ID
    """
    fsrar_id = ''
    try:
        utm_page_text = requests.get(utm_url).text
        # поищем на странице строку типа 'FSRAR-RSA-020000593580_E' и заберем оттуда ID
        fsrar_id = re.search('FSRAR-RSA-\d{12}', utm_page_text).group(0)[10:]
    except Exception:
        err_stack = traceback.format_exc()
        logger.error(err_stack)
    return fsrar_id


def send_utm_document(root: ET.Element, url, filename):
    '''
    Отправка документа в УТМ
    :param root: ET-элемент для отправки
    :param url: полный url для отправки типа 'http://localhost:8080/opt/in/QueryClients_v2'
    :param filename: ни к чему не обязывающая строка - название файла для хэдера запроса
    :return: boolean True, если отправка состоялась, replyID (текст из тэга <url> ответа)
    '''
    replyid = ''
    try:
        xmltext = prettify(root).decode('utf-8')
        boundary = str(uuid.uuid4())
        content = '--' + boundary + '\n' + \
                  f'Content-Disposition: form-data; name="xml_file"; filename=\"{filename}\"' + '\n' + \
                  'Content-Type: text/xml; charset=utf-8 \n\n' + \
                  xmltext + '\n' + \
                  '--' + boundary + '--\n'
        headers = {"Content-type": "multipart/form-data; boundary=" + boundary,
                   "Content-Length": str(len(content)),
                   "Accept": "text/xml"}
        r = requests.request("POST", url, data=content.encode('utf-8'), headers=headers)
        # надо распарсить ответ
        resp = ET.fromstring(r.content.decode('utf-8'))
        urltag = resp.find('./url')
        errtag = resp.find('./error')
        if errtag is None:
            if urltag is not None:
                return True, urltag.text
            else:
                logger.error(f'Document {filename} was NOT sended to {url} with responce: {resp.text}')
        else:
            logger.error(f'Document {filename} was NOT sended to {url} with UTM error: {errtag.text}')
            return False, errtag.text
    except Exception:
        err_stack = traceback.format_exc()
        logger.error(f'Error sending POST to {url} with content \n {content}')
        logger.error(err_stack)


