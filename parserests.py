import os
from external import send_utm_document, get_utm_documents
from utils import spaced, prettify, app_log, f2_already_collected
import time
from xml_work import create_QueryRestBCode, write_reply_to_file, create_ActWriteOff_v3
import traceback
from timeloop import Timeloop
from datetime import timedelta, datetime
import pandas as pd
from pathlib import Path
from random import randint
import csv

OWNER_FSRAR_ID = '020000593580'
UTM_URL = ' http://94.141.190.142:18080'
# читаем файл с F2
script_dir = os.path.dirname(os.path.abspath(__file__))


def QueryRestBCode(checkfile):
    try:
        for f2 in open('f2.txt').read().splitlines():
            # проверим, не записаны ли уже марки
            if f2_already_collected(f2, checkfile):
                continue
            else:
                query = create_QueryRestBCode(f2, OWNER_FSRAR_ID)
                send, reply = send_utm_document(query, UTM_URL + '/opt/in/QueryRestBCode', 'QueryRestBCode')
                if send:
                    app_log.info(f'Запрос по F2={f2}  отправлен!')
                    app_log.info(f'Ответ УТМ:' + reply)
                else:
                    app_log.error(f'Запрос по F2={f2} не отправлен!')
                    app_log.error(f'Ответ УТМ:' + reply)
                break
    except Exception:
        err_stack = traceback.format_exc()
        app_log.error(err_stack)


def ReplyRestBCodesToFile():
    for url in get_utm_documents(UTM_URL + '/opt/out', 'ReplyRestBCode')['urls']:
        #print(url)
        write_reply_to_file(url, 'f2_marks.csv')


def send_act():
    '''
    Создает и посылает акты списания
    Записывает отосланные f2 в отдельный файл
    :return:
    '''
    reportfile = 'created_acts.csv'
    # можно было бы попробовать pandas join, чтобы быстро выковыривать строки.
    path = Path('.')
    df_rest = pd.read_csv('f2_marks.csv', header=None, names=['date', 'f2', 'mark'])
    df_created = None
    if os.path.isfile('created_acts.csv'):
        df_created = pd.read_csv('created_acts.csv', header=None, names=['date', 'f2', 'mark'])
        # print(df_created.head())
    # если уже что-то списывали, списанное лежит в df_created
    # берем по N марок в акт
    n = randint(300, 500)
    if df_created is not None:
        # trick from sof
        df_to_act = df_rest[~df_rest.isin(df_created)].dropna()[:n]
    else:
        df_to_act = df_rest[:n]
    # если есть что списывать
    if df_to_act.shape[0] > 0:
        # возьмем сгруппированные по f2 датасеты и кинем их в функцию формирования акта
        df_by_f2 = [g[1] for g in list(df_to_act.groupby('f2'))]
        # посчитаем файлы ActWriteOff_v3 для счетчика
        fcnt = sum(1 for x in path.glob('ActWriteOff*.*') if x.is_file())
        act = create_ActWriteOff_v3(df_by_f2, OWNER_FSRAR_ID, str(2000+fcnt+1).zfill(8))
        # print(prettify(act).decode('utf-8'))
        send, reply = send_utm_document(act, UTM_URL + '/opt/in/ActWriteOff_v3', 'ActWriteOff_v3')
        if send:
            app_log.info(f'Акт списания ActWriteOff_v3_{str(fcnt)}.xml отправлен!')
            app_log.info(f'Ответ УТМ:' + reply)
            df_to_act.to_csv('created_acts.csv', mode='a', header=None)
            with open('ActWriteOff_v3_'+str(fcnt)+'.xml', 'wb') as f:
                f.write(prettify(act).decode('utf-8').encode("utf8"))
        else:
            app_log.error(f'Акт списания НЕ отправлен!')
            app_log.error(f'Ответ УТМ:' + reply)

    return


tl = Timeloop()


@tl.job(interval=timedelta(seconds=630))
def SendQueryEvery10Minutes():
    QueryRestBCode('f2_marks.csv')
    print("QueryRestBCode time : {}".format(time.ctime()))


@tl.job(interval=timedelta(seconds=300))
def GetRepliesEvery30Minutes():
    ReplyRestBCodesToFile()
    print("ReplyRestBCodesToFile time : {}".format(time.ctime()))


@tl.job(interval=timedelta(seconds=4*60*60))
def SendActs():
    send_act()
    print("SendActs time : {}".format(time.ctime()))


# планировщик
if __name__ == "__main__":
    # QueryRestBCode('f2_marks.csv')
    # ReplyRestBCodesToFile()
    QueryRestBCode('f2_marks.csv')
    ReplyRestBCodesToFile()
    send_act()
    tl.start(block=True)

