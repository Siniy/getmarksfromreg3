import xml.etree.ElementTree as ET
from xml.dom import minidom
import logging
from logging.handlers import RotatingFileHandler
import os
import csv

log_formatter = logging.Formatter('%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s')
script_dir = os.path.dirname(os.path.abspath(__file__))
logFile = 'act.log'

my_handler = RotatingFileHandler(logFile,  maxBytes=10*1024*1024,
                                 backupCount=2, delay=0, encoding='utf-8')
my_handler.setFormatter(log_formatter)
my_handler.setLevel(logging.DEBUG)

app_log = logging.getLogger('root')
app_log.addHandler(my_handler)
app_log.setLevel(logging.DEBUG)


def spaced(tag, namespace):
    '''
    Добавление namespace к тэгу
    '''
    return '{' + namespace + '}' + tag


def prettify(root: ET.Element):
    """
    Используется для красивого отображения XML
    :param root: Корень отображаемого дерева
    :return:
    """
    rough_string = ET.tostring(root, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent='    ', encoding='utf-8')


def f2_already_collected(f2, checkfile):
    '''
    Поиск параметра F2 в первом столбце csv-файла checkfile
    :param f2:
    :param checkfile:
    :return: True, если найден
    '''
    if os.path.isfile(checkfile):
        for row in csv.reader(open(checkfile, 'r'), delimiter=","):
            if f2 == row[1]:
                return True
    return False


def act_already_created(f2, checkfile):
    '''
    Поиск параметра F2 в первом столбце csv-файла checkfile
    :param f2:
    :param checkfile:
    :return: True, если найден
    '''
    if os.path.isfile(checkfile):
        for row in csv.reader(open(checkfile, 'r'), delimiter=","):
            if f2 == row[0]:
                return True
    return False
