import os
import external
import csv
import xml.etree.cElementTree as ET
import traceback
from utils import spaced, prettify, app_log
import requests
import pandas as pd
from datetime import datetime
import uuid

ns = {
    'c': 'http://fsrar.ru/WEGAIS/Common',
    'ce': 'http://fsrar.ru/WEGAIS/CommonV3',
    'iab': 'http://fsrar.ru/WEGAIS/ActInventoryF1F2Info',
    'ns': 'http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01',
    'oref': 'http://fsrar.ru/WEGAIS/ClientRef_v2',
    'pref': 'http://fsrar.ru/WEGAIS/ProductRef_v2',
    'wb': 'http://fsrar.ru/WEGAIS/TTNSingle_v3',
    'xs': 'http://www.w3.org/2001/XMLSchema',
    'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
    'wbr': 'http://fsrar.ru/WEGAIS/TTNInformF2Reg',
    'wa': 'http://fsrar.ru/WEGAIS/ActTTNSingle_v3',
    'qp': 'http://fsrar.ru/WEGAIS/QueryParameters',
    'rap': 'http://fsrar.ru/WEGAIS/ReplyAP_v2',
    'awr': "http://fsrar.ru/WEGAIS/ActWriteOff_v3",
    'rst': "http://fsrar.ru/WEGAIS/ReplyRestBCode"
}


def create_QueryRestBCode(f2: str, owner_fsrar_id) -> ET.Element:
    '''
    создаем запрос по F2 для owner_fsrar_id
    :return: xml запроса
    '''
    # зарегистрируем неймспейсы для создания элементов
    app_log.info(f"Создание запроса для F2={f2}")
    try:
        for prefix in ns:
            ET.register_namespace(prefix, ns[prefix])

        # spaced добавляет неймспейс к названию тэга, а register_namespace заменит его префиксом
        queryroot = ET.Element(spaced('Documents', ns['ns']))
        queryroot.set('Version', '1.0')

        owner = ET.SubElement(queryroot, spaced('Owner', ns['ns']))
        fsrar_id_el = ET.SubElement(owner, spaced('FSRAR_ID', ns['ns']))
        fsrar_id_el.text = owner_fsrar_id
        document = ET.SubElement(queryroot, spaced('Document', ns['ns']))
        QueryRestBCode = ET.SubElement(document, spaced('QueryRestBCode', ns['ns']))
        Parameters = ET.SubElement(QueryRestBCode, spaced('Parameters', ns['qp']))
        Parameter = ET.SubElement(Parameters, spaced('Parameter', ns['qp']))
        Name = ET.SubElement(Parameter, spaced('Name', ns['qp']))
        Name.text = 'ФОРМА2'
        Value = ET.SubElement(Parameter, spaced('Value', ns['qp']))
        Value.text = f2
        return queryroot
    except Exception:
        err_stack = traceback.format_exc()
        app_log.error(err_stack)


def write_reply_to_file(url, wfile):
    '''
    Пишем ответ из ReplyRestBCode в файл с парами F2,mark
    :param url:
    :param wfile:
    :return:
    '''
    try:
        reply = ET.fromstring(requests.get(url).text)
        ReplyRestBCode = reply.find(".//ns:ReplyRestBCode", ns)
        # positions = ReplyRestBCode.findall("./rst:MarkInfo/ce:amc", ns)
        f2 = ReplyRestBCode.find("./rst:Inform2RegId", ns).text
        if os.path.isfile(wfile):
            for row in csv.reader(open(wfile, 'r'),  delimiter=","):
                if f2 == row[1]:
                    #app_log.debug(f"В файле {wfile} уже есть инфо по справке f2={f2}")
                    return
        csv_file = open(wfile,  'a+', newline='')
        csv_writer = csv.writer(csv_file)
        if len(ReplyRestBCode.findall("./rst:MarkInfo/ce:amc", ns))==0:
            csv_writer.writerow((str(datetime.now().date()), f2, 'NOMARK'))
        else:
            for mark in ReplyRestBCode.findall("./rst:MarkInfo/ce:amc", ns):
                csv_writer.writerow((str(datetime.now().date()), f2, mark.text))
        csv_file.close()
        app_log.debug(f"ReplyRestBCode из url={url} для f2={f2} записан в файл {wfile}")
    except Exception:
        err_stack = traceback.format_exc()
        app_log.error(err_stack)


def create_ActWriteOff_v3(df_by_f2, fsrar_id, act_number) -> ET.Element:
    try:
        for prefix in ns:
            ET.register_namespace(prefix, ns[prefix])
        # spaced добавляет неймспейс к названию тэга, а register_namespace заменит его префиксом
        actroot = ET.Element(spaced('Documents', ns['ns']))
        actroot.set('Version', '1.0')
        owner = ET.SubElement(actroot, spaced('Owner', ns['ns']))
        fsrar_id_el = ET.SubElement(owner, spaced('FSRAR_ID', ns['ns']))
        fsrar_id_el.text = fsrar_id
        document = ET.SubElement(actroot, spaced('Document', ns['ns']))
        ActWriteOff_v3 = ET.SubElement(document, spaced('ActWriteOff_v3', ns['ns']))
        act_identity = ET.SubElement(ActWriteOff_v3, spaced('Identity', ns['awr']))
        act_identity.text = 'awo-' + str(uuid.uuid4())[:8]
        Header = ET.SubElement(ActWriteOff_v3, spaced('Header', ns['awr']))
        ActNumber = ET.SubElement(Header, spaced('ActNumber', ns['awr']))
        ActNumber.text = act_number
        ActDate = ET.SubElement(Header, spaced('ActDate', ns['awr']))
        ActDate.text = str(datetime.now().date())
        TypeWriteOff = ET.SubElement(Header, spaced('TypeWriteOff', ns['awr']))
        TypeWriteOff.text = 'Недостача'
        Note = ET.SubElement(Header, spaced('Note', ns['awr']))
        Content = ET.SubElement(ActWriteOff_v3, spaced('Content', ns['awr']))
        for idx, f2_set in enumerate(df_by_f2):
            if len(f2_set.iloc[0]['mark']) > 70:
                f2 = f2_set.iloc[0]['f2']
                Position = ET.SubElement(Content, spaced('Position', ns['awr']))
                pos_identity = ET.SubElement(Position, spaced('Identity', ns['awr']))
                pos_identity.text = str(idx+1)
                Quantity = ET.SubElement(Position, spaced('Quantity', ns['awr']))
                Quantity.text = str(f2_set.shape[0])
                InformF1F2 = ET.SubElement(Position, spaced('InformF1F2', ns['awr']))
                InformF2 = ET.SubElement(InformF1F2, spaced('InformF2', ns['awr']))
                F2RegId = ET.SubElement(InformF2, spaced('F2RegId', ns['pref']))
                F2RegId.text = f2
                MarkCodeInfo = ET.SubElement(Position, spaced('MarkCodeInfo', ns['awr']))
                for index, row in f2_set.iterrows():
                    amc = ET.SubElement(MarkCodeInfo, spaced('amc', ns['ce']))
                    amc.text = row['mark']
        return actroot
    except Exception:
        err_stack = traceback.format_exc()
        app_log.error(err_stack)

    # mark_data = pd.read_csv('f2_marks.csv', header = 0, delimiter = ',')
    # act_already_created()